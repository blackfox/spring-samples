package com.spring.mybatis;

import com.spring.mybatis.conf.AppConfig;
import com.spring.mybatis.service.UserService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author yangjian
 */
public class Application {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(AppConfig.class);


		context.refresh();

		UserService userService = context.getBean("userService", UserService.class);
		userService.test();
	}
}
