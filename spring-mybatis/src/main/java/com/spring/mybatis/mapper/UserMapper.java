package com.spring.mybatis.mapper;

import org.apache.ibatis.annotations.Select;

/**
 * @author yangjian
 */
public interface UserMapper {

	@Select("select 'User'")
	String selectUser();
}
