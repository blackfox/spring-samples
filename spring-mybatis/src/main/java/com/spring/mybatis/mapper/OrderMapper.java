package com.spring.mybatis.mapper;

import org.apache.ibatis.annotations.Select;

/**
 * @author yangjian
 */
public interface OrderMapper {

	@Select("select 'Order'")
	String selectUser();
}
