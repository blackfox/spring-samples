package com.spring.mybatis.mapper;

import org.apache.ibatis.annotations.Select;

/**
 * @author yangjian
 */
public interface LogMapper {

	@Select("select 'Log'")
	String selectUser();
}
