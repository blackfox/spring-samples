package com.spring.mybatis.service;

import com.spring.mybatis.mapper.LogMapper;
import com.spring.mybatis.mapper.OrderMapper;
import com.spring.mybatis.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author yangjian
 */
@Component
public class UserService {

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private OrderMapper orderMapper;

	@Autowired
	private LogMapper logMapper;

	public void test() {
		System.out.println(userMapper.selectUser());
		System.out.println(orderMapper.selectUser());
		System.out.println(logMapper.selectUser());
	}
}
