package com.spring.controller;

import com.spring.controller.vo.User;
import com.spring.mvc.ResponseBody;
import com.spring.mvc.annotation.Autowired;
import com.spring.mvc.annotation.Controller;
import com.spring.mvc.annotation.GetMapping;
import com.spring.mvc.annotation.RequestMapping;
import com.spring.service.UserService;

import javax.servlet.http.HttpServletRequest;

/**
 * @author yangjian
 */
@Controller
public class UserController {

	@Autowired("userService")
	private UserService userService;

	@RequestMapping(value = "/hello", method = RequestMapping.GET)
	public String hello()
	{
		return "hello, world";
	}

	@GetMapping("/user")
	@ResponseBody
	public User getUser(HttpServletRequest request, String name)
	{
		System.out.println(name);
		String username = request.getParameter("username");
		if (null == username) {
			username = "RockYang";
		}
		return new User(username, 18);
	}
}
