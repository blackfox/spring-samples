package com.spring.service;

import com.spring.mvc.annotation.Service;

/**
 * @author yangjian
 */
@Service("userService")
public class UserServiceImpl implements UserService {
	@Override
	public void test()
	{
		System.out.println("=========== invoke UserService.test() ================");
	}
}
