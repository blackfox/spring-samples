package com.spring.mvc;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yangjian
 */
public class ApplicationContext {

	// Bean 容器单例池
	private final Map<String/*beanName*/, Object/*beanObject*/> singletonObjects;

	public ApplicationContext()
	{
		singletonObjects = new ConcurrentHashMap<>();
	}

	public void registerBean(String beanName, Object value)
	{
		if (singletonObjects.containsKey(beanName)) {
			throw new RuntimeException("bean " + beanName + " is already exists");
		}
		singletonObjects.put(beanName, value);
	}

	public Object getBean(String beanName)
	{
		return singletonObjects.get(beanName);
	}

	public Map getBeans()
	{
		return singletonObjects;
	}
}
