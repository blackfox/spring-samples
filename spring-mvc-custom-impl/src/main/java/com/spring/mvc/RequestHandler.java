package com.spring.mvc;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * Http 请求处理 Handler
 *
 * @author yangjian
 */
public class RequestHandler {

	private final String path;
	private final Object controller;
	private final Method method;
	private final String httpMethod;

	public RequestHandler(String path, Object controller, Method method, String httpMethod)
	{
		this.path = path;
		this.controller = controller;
		this.method = method;
		this.httpMethod = httpMethod;
	}

	public Object getController()
	{
		return controller;
	}

	public Method getMethod()
	{
		return method;
	}

	public boolean match(HttpServletRequest request)
	{
		return request.getRequestURI().equals(path) && request.getMethod().equals(httpMethod);
	}
}
