package com.spring.mvc;

import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

/**
 * @author yangjian
 */
public class MyTest {

	// 测试获取参数名称
	@Test
	public void test() throws NoSuchMethodException
	{
		for (Method method : Service.class.getDeclaredMethods()) {
			Parameter[] parameters = method.getParameters();
			for (Parameter parameter : parameters) {
				System.out.println(parameter.getType());
			}
		}
	}
}
