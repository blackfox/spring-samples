package com.dubbo.provider;

import com.dubbo.framework.http.HttpServer;

/**
 * @author yangjian
 **/
public class ProviderStarter
{
    public static void main(String[] args)
    {
        new HttpServer().start("localhost", 8080);
    }
}
