package com.dubbo.provider.api;

/**
 * @author yangjian
 **/
public interface UserService
{
    String hello(String name);
}
