package com.dubbo.provider.impl;

import com.dubbo.provider.api.UserService;

/**
 * @author yangjian
 **/
public class UserServiceImpl implements UserService
{
    @Override
    public String hello(String name)
    {
        return "hello, " + name;
    }
}
