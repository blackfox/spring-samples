package com.dubbo.framework.http;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author yangjian
 **/
public class DispatcherServlet extends HttpServlet
{
    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp)
    {
        new HttpRequestHandler().handle(req, resp);
    }
}
