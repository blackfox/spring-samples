package com.dubbo.framework.http;

import com.dubbo.framework.protocol.Invocation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * @author yangjian
 **/
public class HttpRequestHandler
{
    public void handle(HttpServletRequest req, HttpServletResponse resp) throws IOException, ClassNotFoundException
    {
        // 解析调用请求
        Invocation invocation = (Invocation) new ObjectInputStream(req.getInputStream()).readObject();
    }
}
