package org.spring.demo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

	public static void main(String[] args)
	{
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

		Object orderServiceFactoryBean = context.getBean("orderServiceFactoryBean");
		System.out.println(orderServiceFactoryBean);


	}

}
