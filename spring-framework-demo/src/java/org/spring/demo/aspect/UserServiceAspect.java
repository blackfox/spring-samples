package org.spring.demo.aspect;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @author yangjian
 */
@Aspect
@Component
public class UserServiceAspect {

	@Before("execution(public void org.spring.demo.service.UserService.test())")
	public void testBefore(JoinPoint joinPoint) {
		System.out.printf("Invoke Before: %s%n", joinPoint.getTarget());
	}
}
