package org.spring.demo.event;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author yangjian
 */
@Component
public class MyEventListener {

	@EventListener(UserEvent.class)
	public void newUser(UserEvent event)
	{
		System.out.println("收到用户注册事件： " + event.getUser());
	}
}
