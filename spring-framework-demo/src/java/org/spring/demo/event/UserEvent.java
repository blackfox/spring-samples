package org.spring.demo.event;

import org.spring.demo.domain.User;

/**
 * @author yangjian
 */
public class UserEvent {
	private User user;


	public UserEvent(User user)
	{
		this.user = user;
	}

	public User getUser()
	{
		return user;
	}
}
