package org.spring.demo.service;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author yangjian
 */
@Component("orderServiceFactoryBean")
@Scope("prototype")
public class OrderServiceFactoryBean implements FactoryBean {
	@Override
	public Object getObject() throws Exception
	{
		return new OrderService();
	}

	@Override
	public Class<?> getObjectType()
	{
		return OrderService.class;
	}
}
