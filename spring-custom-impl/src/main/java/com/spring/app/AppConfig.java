package com.spring.app;

import com.spring.framework.ComponentScan;

/**
 * @author yangjian
 */
@ComponentScan(values = {"com.spring.app.service", "com.spring.app.processor"})
public class AppConfig {
}
