package com.spring.app.processor;

import com.spring.app.service.Value;
import com.spring.framework.BeanPostProcessor;
import com.spring.framework.BeanProcessor;

import java.lang.reflect.Field;

/**
 * @author yangjian
 */
@BeanProcessor
public class MyBeanPostProcessor implements BeanPostProcessor {
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws Exception
	{
		System.out.printf("Invoked Before bean %s initialization...%n", beanName);
		// 这里可以实现 @Value 注解
		for (Field field : bean.getClass().getDeclaredFields()) {
			if (field.isAnnotationPresent(Value.class)) {
				field.setAccessible(true);
				field.set(bean, field.getAnnotation(Value.class).value());
			}
		}
		return bean;
	}

}
