package com.spring.app;

import com.spring.app.service.UserService;
import com.spring.framework.AnnotationConfigApplicationContext;

/**
 * @author yangjian
 */
public class Application {
	public static void main(String[] args)
	{
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
		UserService userService1 = (UserService) applicationContext.getBean("userService");
		UserService userService2 = (UserService) applicationContext.getBean("userService");
		UserService userService3 = (UserService) applicationContext.getBean("userService");
		userService1.test();
	}
}
