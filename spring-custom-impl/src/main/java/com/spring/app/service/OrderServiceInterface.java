package com.spring.app.service;

/**
 * @author yangjian
 */
public interface OrderServiceInterface {
	void pay();
}
