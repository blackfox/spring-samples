package com.spring.app.service;

import com.spring.framework.Component;
import com.spring.framework.Scope;

/**
 * @author yangjian
 */
@Component
@Scope
public class OrderService implements OrderServiceInterface {
	@Override
	public void pay()
	{
		System.out.println("You are paid $10000, thank you!");
	}
}
