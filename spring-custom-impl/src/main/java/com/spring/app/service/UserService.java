package com.spring.app.service;

import com.spring.framework.AutoWired;
import com.spring.framework.Component;
import com.spring.framework.Scope;

/**
 * @author yangjian
 */
@Component("userService")
@Scope(Scope.PROTOTYPE)
public class UserService {

	@Value("xiaoMing")
	private String username;
	@AutoWired
	private OrderServiceInterface orderService;

	public void test()
	{
		System.out.println("Hello UserService " + username);
		orderService.pay();
	}
}
