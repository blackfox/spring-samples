package com.spring.framework;

/**
 * @author yangjian
 */
public interface InitializingBean {
	void afterPropertiesSet() throws Exception;
}
